# Awesome POP [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/saltstack/pop-awesome)

A curated list of awesome POP projects, plugins, and resources.


## Table of Contents
- [Awesome POP](#awesome-pop)
    - [POP](#pop)
    - [Acct](#acct)
    - [Grains](#grains)
    - [Idem](#idem)
    - [Idem Platform](#idem-platform)
    - [Idem Cloud](#idem-cloud)
    - [Idem Salt](#idem-salt)
    - [Packaging](#packaging)
    - [Testing](#testing)
    - [Funtoo](#funtoo)
    - [Awesome Asyncio](#awesome-asyncio)
    
---

### [POP](https://gitlab.com/saltstack/pop/pop)

Plugin Oriented Programming Paradigm

* [Documentation](https://pop.readthedocs.io)
* [POP-book](https://gitlab.com/saltstack/pop/pop-book) - A book about the philosophy and how to think in Plugin Oriented Programming

#### Blogs
* [Pop Culture: Plugin Oriented Programming for Infrastructure Automation](https://www.saltstack.com/blog/pop-culture-plugin-oriented-programming-for-infrastructure-automation/)

#### News
* [SaltStack: Plugin-Oriented Programming Could Help Open Source Woes](https://thenewstack.io/saltstack-plugin-oriented-programming-could-help-open-source-woes/)
* [SaltStack POP: Code More and Forget About Those Pesky YAML Files](https://thenewstack.io/saltstack-pop-code-more-and-forget-about-those-pesky-yaml-files/)
* [SaltStack Introduces Plugin Oriented Programming with New Open-Source Innovation Modules to Power Scalable Automation and Artificial Intelligence ](https://www.prnewswire.com/news-releases/saltstack-introduces-plugin-oriented-programming-with-new-open-source-innovation-modules-to-power-scalable-automation-and-artificial-intelligence-300987194.html)
* [ITOps Times Open-Source Project(s) of the Week: Heist, Umbra, and Idem](https://www.itopstimes.com/itops/itops-times-open-source-projects-of-the-week-heist-umbra-and-idem/)

#### Podcasts
* [Why Do Programming Paradigms Matter?](https://www.saltstack.com/the-hacks/episodes/why-do-programming-paradigms-matter/)
* [Plugin Oriented Pop, Umbra, Idem & Heist OH MY!](https://www.saltstack.com/the-hacks/episodes/plugin-oriented-pop-umbra-idem-heist-oh-my/)
* [Making Complex Software Fun And Flexible With Plugin Oriented Programming](https://www.pythonpodcast.com/plugin-oriented-programming-episode-240/)

#### Videos
* [Plugin Oriented Programming](https://www.youtube.com/watch?v=IyI6rmVFfUM)
* [Plugin Oriented Programming A New Way to Scale Development](https://www.youtube.com/watch?v=cavGklX54B0)

#### Examples
* [Poppy](https://gitlab.com/saltstack/pop/poppy) - An RPC server to help people learn POP


#### Projects
* [Config](https://gitlab.com/saltstack/pop/pop-config) - The app merging, super simple, complete configuration loading and documenting framework which facilitates foundational capabilities inside of Plugin Oriented Programming
* [Heist](https://gitlab.com/saltstack/pop/heist) - Ephemeral software tunneling and delivery system
    - [Example](https://github.com/nicholasmhughes/heist-example)
* [Rend](https://gitlab.com/saltstack/pop/rend) - A collection of tools to render text files into data structures
* [Takara](https://gitlab.com/saltstack/pop/takara) - Easy to use secret store with varying levels of security vs ease of use options
* [Umbra](https://gitlab.com/saltstack/pop/umbra) - AI/ML made easy


### [Acct](https://gitlab.com/saltstack/pop/acct)

Simple and secure account management

* [Acct Backends](https://gitlab.com/Akm0d/acct-backends) - Extensions to acct so that providers and profiles can be defined in alternate secret stores (Such as lastpass)


### [Idem](https://gitlab.com/saltstack/pop/idem)

Transform configuration into idempotent action

* [Documentation](https://idem.readthedocs.io)

#### Projects

* [Idem ReadTheDocs](https://gitlab.com/saltstack/pop/idem-readthedocs) - Idem  support for management of documentation hosted on ReadTheDocs.
    
    

### [Grains](https://gitlab.com/saltstack/pop/corn)

System information discovery and asset tracking

* [Grains Universal](https://gitlab.com/Akm0d/grains-universal) - Pure python grains that are available on every OS
* [Grains Virtualization](https://gitlab.com/saltstack/pop/corn_virtualiztion) - Virtualization grains
* [Grains Cowsay](https://gitlab.com/Akm0d/corn_cowsay) - A silly grains extension that shows hwo easy it is to extend grainsv2


### Idem Platform

Management of specific platforms with grains, execution modules, and state modules

* [Idem Platform Meta](https://gitlab.com/Akm0d/idem-grains) - Meta package for installing the correct idem-[platform] project for your system
* [Idem AIX](https://gitlab.com/saltstack/pop/idem-aix)
* [Idem BSD](https://gitlab.com/saltstack/pop/idem-bsd)
* [Idem Darwin](https://gitlab.com/saltstack/pop/idem-darwin)
    - [Working Group Discussion](https://www.youtube.com/watch?v=kCqEqulk-Eo) - 2020/06/16
* [Idem Linux](https://gitlab.com/saltstack/pop/idem-linux)
* [Idem Posix](https://gitlab.com/Akm0d/idem-posix)
* [Idem Solaris](https://gitlab.com/saltstack/pop/idem-solaris)
* [Idem Windows](https://gitlab.com/saltstack/pop/idem-windows)
    - [Working Group Discussion](https://www.youtube.com/watch?v=5W1ppO3exl4) - 2020/06/16


### Idem Cloud
* [Idem Cloud](https://gitlab.com/saltstack/pop/idem-cloud) - Generic cloud contracts, options, and bootstrapping agent.
    - [Documentation](https://gitlab.com/saltstack/pop/idem-cloud/-/blob/master/migrate_salt_cloud.rst)
* [Idem Azurearm](https://github.com/eitrtechnologies/idem-azurerm) - Microsoft Azure Cloud Provider for Idem
    - [Documentation](https://idem-azurerm.readthedocs.io/en/latest/)
    - [Introuction](https://github.com/nicholasmhughes/idem-azurerm-intro)
    - [Getting Started](https://eitr.tech/blog/2020/05/07/Getting-Started-with-Idem-for-Microsoft-Azure.html)
    - [Version 2.1 Update](https://eitr.tech/blog/2020/05/15/idem-for-microsoft-azure-2.1-update.html)
    - [Devops Columbia Presntation](https://www.youtube.com/watch?v=oo24caqVgMk) - Infrastructure as code with Idem
* [Idem AWS](https://gitlab.com/saltstack/pop/idem-aws) - AWS Provider for Idem
    - [Working Group Discussion](https://www.youtube.com/watch?v=ewtLUrIyTZU) - 2020/06/12
    - [Working Group Discussion](https://www.youtube.com/watch?v=E9PihmZ1la8) - 2020/07/13
    - [Working Group Discussion](https://www.youtube.com/watch?v=maEgz95ovCw) - 2020/08/10
* [Idem Libvirt](https://github.com/cbosdo/idem_provider_libvirt) - Libvirt Provider for Idem 
* [Idem LXD](https://github.com/UtahDave/idem-lxd) - LXD Provider for Idem 
* [Idem Vagrant](https://gitlab.com/saltstack/pop/idem-vagrant) - Vagrant Provider for Idem 
* [Idem VirtualBox](https://gitlab.com/saltstack/pop/idem-virtualbox) - Virtualbox Provider for Idem 
* [Idem Vultr](https://gitlab.com/saltstack/pop/idem-vultr) - Vultr Provider for Idem 


### Idem Salt
Since Magnesium, Idem states and execution modules can be called from within salt!
* [Module](https://docs.saltstack.com/en/master/ref/modules/all/salt.modules.idem.html) - Salt module to call idem exec modules
* [State](https://docs.saltstack.com/en/master/ref/states/all/salt.states.idem.html) - Salt state to call idem states

### Packaging
* [Bodger](https://gitlab.com/saltstack/pop/bodger) - A high level command to split out tasks based on running platform and command
* [Pkgr](https://gitlab.com/saltstack/pop/pkgr) - Simple tool to make building more complex packages with pop-build easy
* [Pop Release](https://gitlab.com/saltstack/pop/pop-release) - A helper tool for cutting releases of POP based projects
* [Tiamat](https://gitlab.com/saltstack/pop/tiamat) - Used to create single binaries of POP and Python projects

### Testing
* [Idem Validator](https://github.com/mchugh19/idem_validator) - Validator is an implementation of saltcheck written for pop and idem.
* [Pytest-pop](https://gitlab.com/saltstack/pop/pytest-pop) - A pytest plugin to make unit/integration tests with a hub easy
* [Test Generator](https://gitlab.com/Akm0d/pop-utils) - Tool that generates boiler plate code for testing a pop project

### Funtoo Linux
* [Metatools](https://code.funtoo.org/bitbucket/users/drobbins/repos/funtoo-metatools/) - (Autogen) Automate the creation of ebuilds.
* [Ego](https://github.com/funtoo/ego) - Configuration and management meta-tool. 

### Awesome Asyncio
* [Awesome Asyncio](https://github.com/timofurrer/awesome-asyncio) - A curated list of awesome Python asyncio frameworks, libraries, software and resources
* [Asyncio Third Party libs](https://github.com/python/asyncio/wiki/ThirdParty) - A list of libraries for AsyncIO is available on PyPI with Framework::AsyncIO classifier.


## Got ideas?
Open an issue in this repo for a your new pop-project idea